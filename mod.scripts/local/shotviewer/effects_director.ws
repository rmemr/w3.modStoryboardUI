// ----------------------------------------------------------------------------

class CModStoryBoardEffectsDirector {
    // ------------------------------------------------------------------------
    // defines what assets are available
    private var assets : array<CModStoryBoardAsset>;
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    public function setAssets(assets: array<CModStoryBoardAsset>) {
        this.assets = assets;
    }
    // ------------------------------------------------------------------------
    public function stopAllEffectsForAsset(asset: CModStoryBoardAsset) : bool {
        var entity : CEntity = asset.getEntity();
        if (!entity)
            return false;

        entity.StopAllEffects();
        return true;
    }
    // ------------------------------------------------------------------------
    public function startEffectForAsset(asset: CModStoryBoardAsset, stopPrevious: bool) : bool {
        var shotSettings    : SStoryBoardShotAssetSettings = asset.getShotSettings();
        var effectSettings  : SStoryBoardEffectsSettings   = shotSettings.effects;
        var entity          : CEntity                      = asset.getEntity();

        if (!entity)
            return false;

        if ( effectSettings.effectName == 'None' ) {
            entity.StopAllEffects();
            return true;
        }
        if (stopPrevious) {
            entity.StopAllEffects();
        }
        if (entity.HasEffect( effectSettings.effectName )) {
            return entity.PlayEffect( effectSettings.effectName );
        } else {
            return false;
        }
    }
    // ------------------------------------------------------------------------
    public function stopEffects() {
        var i : int;
        for (i = 0; i < assets.Size(); i += 1) {
            stopAllEffectsForAsset(assets[i]);
        }
    }
    // ------------------------------------------------------------------------
    public function startShotEffects() {
        var i : int;
        for (i = 0; i < assets.Size(); i += 1) {
            startEffectForAsset(assets[i], true);
        }
    }
}
// ----------------------------------------------------------------------------
