// ----------------------------------------------------------------------------
struct SStoryBoardEffectsInfo {
    var templatePath : String;
    var effectNames  : array<CName>;
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
class CModSbUiEffectsList extends CModUiFilteredList {
    public var effectsList: array<SStoryBoardEffectsInfo>;
    // ------------------------------------------------------------------------
    public function loadCsvs() {
        var data, dataCNAMES     : C2dArray;
        var templatePath         : String;
        var effectsSequence      : String;
        var effectsArray         : array<CName>;
        var cnameIdx             : int;
        var i, j, k, effectsCnt  : int;
        var extraEffects         : array<SStoryBoardEffectsInfo>;

        // --- load effect cnames from separate csv by row index ---
        data = LoadCSV("dlc\storyboardui\data\entity_effects.csv");
        dataCNAMES = LoadCSV("dlc\storyboardui\data\entity_effects_cnames.csv");

        effectsList.Clear();

        for (i = 0; i < data.GetNumRows(); i += 1) {
            templatePath = data.GetValueAt(0, i);
            effectsCnt = StringToInt( data.GetValueAt(1, i) );
            effectsSequence = data.GetValueAt(2, i);

            for (j = 0; j < effectsCnt; j += 1) {
                cnameIdx = StringToInt( StrBeforeFirst(effectsSequence, ":") );
                effectsArray.PushBack( dataCNAMES.GetValueAtAsName(0, cnameIdx) );

                effectsSequence = StrAfterFirst(effectsSequence, ":");
            }
            effectsList.PushBack( SStoryBoardEffectsInfo(templatePath, effectsArray) );
            effectsArray.Clear();
        }

        // --- add extra effects here
        extraEffects = SBUI_getExtraEffects();
        for (i = 0; i < effectsList.Size(); i += 1) {
            for (j = 0; j < extraEffects.Size(); j += 1) {
                if (extraEffects[j].templatePath == "wasAdded")
                    continue;

                if (effectsList[i].templatePath == extraEffects[j].templatePath) {
                    for (k = 0; k < extraEffects[j].effectNames.Size(); k += 1) {
                        effectsList[i].effectNames.PushBack( extraEffects[j].effectNames[k] );
                    }
                    extraEffects[j].templatePath = "wasAdded";
                }

            }
        }
        for (j = 0; j < extraEffects.Size(); j += 1) {
            if (extraEffects[j].templatePath == "wasAdded")
                continue;

            effectsList.PushBack( extraEffects[j] );
        }
    }
    public function loadArray(tempItems : array<SModUiCategorizedListItem>) {
        items.Clear();
        items = tempItems;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Management of effects for assets per storyboard shot.
//
class CModStoryBoardEffectsListsManager {
    // ------------------------------------------------------------------------
    private var effects: CModSbUiEffectsList;
    private var dataLoaded: Bool;
    // ------------------------------------------------------------------------
    public function init() { }
    // ------------------------------------------------------------------------
    private function lazyLoad() {
        effects = new CModSbUiEffectsList in this;
        effects.loadCsvs();
        dataLoaded = true;
    }
    // ------------------------------------------------------------------------
    public function activate() {}
    // ------------------------------------------------------------------------
    public function deactivate() {}
    // ------------------------------------------------------------------------
    public function getEffectsListForAsset(asset : CModStoryBoardAsset) : CModSbUiEffectsList
    {
        var assetEffects : CModSbUiEffectsList;
        var tempItems    : array<SModUiCategorizedListItem>;
        var assetPath    : String;
        var i, j         : int;

        if (!dataLoaded) { lazyLoad(); }

        assetEffects = new CModSbUiEffectsList in this;
        assetPath = asset.getTemplatePath();

        tempItems.PushBack( SModUiCategorizedListItem(
            "-1",
            "-no effect-",
            "",
            "",
            "")
        );

        // --- look for equal template to filter names for current asset ---
        for (i = 0; i < effects.effectsList.Size(); i += 1) {
            if (assetPath == effects.effectsList[i].templatePath) {
                for (j = 0; j < effects.effectsList[i].effectNames.Size(); j += 1) {
                    tempItems.PushBack( SModUiCategorizedListItem(
                        IntToString(i) + ";" + IntToString(j),
                        NameToString( effects.effectsList[i].effectNames[j] ),
                        "",
                        "",
                        "")
                    );
                }
            }
        }
        assetEffects.loadArray( tempItems );

        return assetEffects;
    }
    // ------------------------------------------------------------------------
    public function getEffectName(selectedUiId: String) : CName {
        var i, j    : int;

        if (!dataLoaded) { lazyLoad(); }

        if (selectedUiId == "-1")
            return 'None';

        i = StringToInt( StrBeforeFirst(selectedUiId, ";") );
        j = StringToInt( StrAfterFirst(selectedUiId, ";") );

        return effects.effectsList[i].effectNames[j];
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
