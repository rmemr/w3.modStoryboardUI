// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// NOTE: you can preview effects only after adding actor/item to scene so make sure
// you have added template too (in mod_additional_templates.ws)
// ----------------------------------------------------------------------------
// Add custom effects for templates (actors/items) here:
// ----------------------------------------------------------------------------
function SBUI_getExtraEffects() : array<SStoryBoardEffectsInfo> {
    var extraEffects: array<SStoryBoardEffectsInfo>;
    var effectNames  : array<CName>;

    // --- EXAMPLE OF ADDING EFFECTS TO 2 TEMPLATES --- //
    /*
    effectNames.Clear();
    effectNames.PushBack('fast_trail_blood_fx');
    effectNames.PushBack('oil_beast');
    effectNames.PushBack('runeword_invigoration');
    extraEffects.PushBack(SStoryBoardEffectsInfo("dlc\dlclaespada\data\items\weapons\swords\witcher_steel_espada.w2ent", effectNames));

    effectNames.Clear();
    effectNames.PushBack('just_some_custom_effect1');
    effectNames.PushBack('just_some_custom_effect2');
    extraEffects.PushBack(SStoryBoardEffectsInfo("gameplay\community\community_npcs\skellige\regular\skellige_fisherman.w2ent", effectNames));
    */

    return extraEffects;
}
