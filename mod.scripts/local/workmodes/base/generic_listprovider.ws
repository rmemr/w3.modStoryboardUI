// ----------------------------------------------------------------------------
class CSbUiGenericSettingList extends CModUiFilteredList {
    // ------------------------------------------------------------------------
    public function preselect(optional openCategories: bool) {
        if (items.Size() > 0) {
            selectedId = items[0].id;
            if (openCategories) {
                selectedCat1 = items[0].cat1;
                selectedCat2 = items[0].cat2;
                selectedCat3 = items[0].cat3;
            }
        }
    }
    // ------------------------------------------------------------------------
    private function shorten(input: String) : String {
        // add unknown setting (e.g. current texture setting value)
        if (StrLen(input) > 32) {
            return "..." + StrRight(input, 32);
        } else {
            return input;
        }
    }
    // ------------------------------------------------------------------------
    public function setOrAddSelection(id: String, optional openCategories: bool) : bool {
        var i: int;

        for (i = 0; i < items.Size(); i += 1) {
            if (items[i].id == id) {
                return setSelection(id, openCategories);
            }
        }

        items.PushBack(SModUiCategorizedListItem(id, shorten(id)));
        return setSelection(id, openCategories);
    }
    // ------------------------------------------------------------------------
    public function getSelectedId() : String {
        return selectedId;
    }
    // ------------------------------------------------------------------------
    public function addCustomEntries(entries: array<String>) {
        var topCat: String;
        var i: int;

        for (i = 0; i < entries.Size(); i += 1) {
            items.PushBack(SModUiCategorizedListItem(entries[i], shorten(entries[i])));
        }
    }
    // ------------------------------------------------------------------------
    public function addEntries(entries: array<SModUiCategorizedListItem>) {
        var i: int;

        for (i = 0; i < entries.Size(); i += 1) {
            items.PushBack(entries[i]);
        }
    }
    // ------------------------------------------------------------------------
    public function addEmptyEntry() {
        items.PushBack(SModUiCategorizedListItem("", GetLocStringByKeyExt("SBUI_NoValue")));
    }
    // ------------------------------------------------------------------------
    public function clear() {
        items.Clear();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CSbUiSettingListProvider {
    // ------------------------------------------------------------------------
    private var loadedLists: array<String>;
    private var providerList: array<CSbUiGenericSettingList>;
    // ------------------------------------------------------------------------
    public function getListProvider(listId: String) : CSbUiGenericSettingList {
        var list: CSbUiGenericSettingList;

        if (!loadedLists.Contains(listId)) {
            list = new CSbUiGenericSettingList in this;

            list.clear();

            switch (listId) {
                case "DofAperture":
                case "DofPlane":
                    list.addEntries(getSettingListEntries(listId));
                    break;
            }

            providerList.PushBack(list);
            loadedLists.PushBack(listId);
        }

        return providerList[loadedLists.FindFirst(listId)];
    }
    // ------------------------------------------------------------------------
    protected function getSettingListEntries(listId: String)
        : array<SModUiCategorizedListItem>
    {
        var entries: array<SModUiCategorizedListItem>;

        switch (listId) {
            case "DofPlane":
                entries.PushBack(SModUiCategorizedListItem("Supercloseup", "Supercloseup"));
                entries.PushBack(SModUiCategorizedListItem("Closeup", "Closeup"));
                entries.PushBack(SModUiCategorizedListItem("Semicloseup", "Semicloseup"));
                entries.PushBack(SModUiCategorizedListItem("Medium", "Medium"));
                entries.PushBack(SModUiCategorizedListItem("Wide", "Wide"));
                break;

            case "DofAperture":
                entries.PushBack(SModUiCategorizedListItem("11.605|0.564891",   "focal length: 11.60  distance: 0.56"));
                entries.PushBack(SModUiCategorizedListItem("13.902|0.891304",   "focal length: 13.90  distance: 0.89"));
                entries.PushBack(SModUiCategorizedListItem("15.9625|1.4985",    "focal length: 15.96  distance: 1.49"));
                entries.PushBack(SModUiCategorizedListItem("16.6908|1.63836",   "focal length: 16.69  distance: 1.63"));
                entries.PushBack(SModUiCategorizedListItem("18.8871|2.0979",    "focal length: 18.88  distance: 2.09"));
                entries.PushBack(SModUiCategorizedListItem("19.937|0.998361",   "focal length: 19.93  distance: 0.99"));
                entries.PushBack(SModUiCategorizedListItem("20.6075|2.4975",    "focal length: 20.60  distance: 2.49"));
                entries.PushBack(SModUiCategorizedListItem("28.2495|1.27007",   "focal length: 28.24  distance: 1.27"));
                entries.PushBack(SModUiCategorizedListItem("31.4669|3.45487",   "focal length: 31.46  distance: 3.45"));
                entries.PushBack(SModUiCategorizedListItem("42.1379|2.70603",   "focal length: 42.13  distance: 2.70"));
                entries.PushBack(SModUiCategorizedListItem("84.4233|1.02439",   "focal length: 84.42  distance: 1.02"));
                break;
        }
        return entries;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------