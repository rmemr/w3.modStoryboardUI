// ----------------------------------------------------------------------------
struct SSBUI_CamSetting {
    var sid: String;
    var caption: String;
    var stepSize: float;
    var min: float;
    var max: float;
}
// ----------------------------------------------------------------------------
class CModSbUiCamSettingsList extends CModUiFilteredList {
    private var settings: array<SSBUI_CamSetting>;
    // ------------------------------------------------------------------------
    public function initList() {
        // note: values are also clamped in cam setters (for backward compatibility)
        items.Clear();
        settings.Clear();

        // csv: CAT1;CAT2;CAT3;id;caption
        settings.PushBack(SSBUI_CamSetting("pos", "position", -1, -1, -1));
        settings.PushBack(SSBUI_CamSetting("rot", "rotation", -1.0, -1.0, -1.0));
        settings.PushBack(SSBUI_CamSetting("fov", "field of view", -5.0, 1.0, 150.0));
        settings.PushBack(SSBUI_CamSetting("zoom", "zoom", 0.2, 1.0, 5.0));
        settings.PushBack(SSBUI_CamSetting("dofIntensity", "dof intensity", 0.1, 0.0, 1.0));
        settings.PushBack(SSBUI_CamSetting("dofFocusFar", "dof focus distance far", 1.0, 0.0, 500.0));
        settings.PushBack(SSBUI_CamSetting("dofFocusNear", "dof focus distance near", 1.0, 0.0, 100.0));
        settings.PushBack(SSBUI_CamSetting("dofBlurFar", "dof blur distance far", 1.0, 0.0, 500.0));
        settings.PushBack(SSBUI_CamSetting("dofBlurNear", "dof blur distance near", 1.0, 0.0, 100.0));
        settings.PushBack(SSBUI_CamSetting("dofAperture", "dof aperture preset", -1, -1, -1));
        settings.PushBack(SSBUI_CamSetting("dofPlane", "dof plane", -1, -1, -1));

        items.PushBack(SModUiCategorizedListItem(0, "position"));
        items.PushBack(SModUiCategorizedListItem(1, "rotation"));
        items.PushBack(SModUiCategorizedListItem(2, "field of view"));
        items.PushBack(SModUiCategorizedListItem(3, "zoom (no preview)"));
        items.PushBack(SModUiCategorizedListItem(4, "dof intensity"));
        items.PushBack(SModUiCategorizedListItem(5, "dof focus distance far"));
        items.PushBack(SModUiCategorizedListItem(6, "dof focus distance near (no preview)"));
        items.PushBack(SModUiCategorizedListItem(7, "dof blur distance far"));
        items.PushBack(SModUiCategorizedListItem(8, "dof blur distance near (no preview)"));
        items.PushBack(SModUiCategorizedListItem(9, "dof aperture preset (no preview)"));
        items.PushBack(SModUiCategorizedListItem(10, "dof plane (no preview)"));
    }
    // ------------------------------------------------------------------------
    public function getSelectedId() : int {
        return StringToInt(selectedId);
    }
    // ------------------------------------------------------------------------
    public function getMeta(id: int) : SSBUI_CamSetting {
        return settings[id];
    }
    // ------------------------------------------------------------------------
    public function getDefaultSettingId(): int {
        // id of fov
        return 2;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// pseudo union
struct SSBUI_Value {
    var type: ESBUI_ValueType;
    var f: Float;
    var i: Int;
    var vec: Vector;
    var s: String;
}
enum ESBUI_ValueType {
    ESBUI_FLOAT = 1,
    ESBUI_INT = 2,
    ESBUI_VECTOR = 3,
    ESBUI_STRING = 4,
}
// ----------------------------------------------------------------------------
class CModStoryBoardCameraParams {
    // ------------------------------------------------------------------------
    private var theCam: CStoryBoardInteractiveCamera;
    // ------------------------------------------------------------------------
    public function setCam(cam: CStoryBoardInteractiveCamera) {
        this.theCam = cam;
    }
    // ------------------------------------------------------------------------
    private function formatFloatPadded(v: float, precision: int) : String {
        var str: String;

        str = FloatToStringPrec(v, precision);
        if (StrFindFirst(str, ".") < 0) {
            str += ".0";
        }
        if (v < 10) {
            return "  " + str;
        } else if (v < 100) {
            return " " + str;
        } else {
            return str;
        }
    }
    // ------------------------------------------------------------------------
    private function formatValue(v: SSBUI_Value) : String {
        var s: string;

        s = GetLocStringByKeyExt("SBUI_lGenericValue") + " ";

        switch (v.type) {
            case ESBUI_VECTOR:
                s = GetLocStringByKeyExt("SBUI_lVectorValue");
                s += formatFloatPadded(v.vec.X, 3) + " / "
                    + formatFloatPadded(v.vec.Y, 3) + " / "
                    + formatFloatPadded(v.vec.Z, 3);
                break;
            case ESBUI_STRING:
                if (StrLen(v.s) > 50) {
                    s += "..." + StrRight(v.s, 50);
                } else {
                    s += v.s;
                }
                break;

            case ESBUI_FLOAT: s += NoTrailZeros(v.f); break;
            case ESBUI_INT: s += IntToString(v.i); break;
            default:
                s = "";
        }
        return s;
    }
    // ------------------------------------------------------------------------
    private function toFloat(value: float) : SSBUI_Value {
        return SSBUI_Value(ESBUI_FLOAT, value);
    }
    // ------------------------------------------------------------------------
    private function toVector(value: Vector) : SSBUI_Value {
        return SSBUI_Value(ESBUI_VECTOR, , , value);
    }
    // ------------------------------------------------------------------------
    private function toAngles(value: EulerAngles) : SSBUI_Value {
        return SSBUI_Value(ESBUI_VECTOR, , , Vector(value.Pitch, value.Yaw, value.Roll));
    }
    // ------------------------------------------------------------------------
    private function toStr(value: String) : SSBUI_Value {
        return SSBUI_Value(ESBUI_STRING, , , , value);
    }
    // ------------------------------------------------------------------------
    private function apertureToStr(focalLength: float, distance: float) : SSBUI_Value {
        return SSBUI_Value(ESBUI_STRING, , , , FloatToString(focalLength) + "|" + FloatToString(distance));
    }
    // ------------------------------------------------------------------------
    public function getValue(id: String) : SSBUI_Value {
        var null: SSBUI_Value;
        var c: array<SCurveDataEntry>;
        var s, i: int;
        var settings: SStoryBoardCameraSettings;

        settings = theCam.getSettings();
        switch (id) {
            case "pos":             return toVector(settings.pos);
            case "rot":             return toAngles(settings.rot);
            case "fov":             return toFloat(settings.fov);
            case "zoom":            return toFloat(settings.zoom);
            case "dofIntensity":    return toFloat(settings.dof.strength);
            case "dofFocusFar":     return toFloat(settings.dof.focusFar);
            case "dofFocusNear":    return toFloat(settings.dof.focusNear);
            case "dofBlurFar":      return toFloat(settings.dof.blurFar);
            case "dofBlurNear":     return toFloat(settings.dof.blurNear);
            case "dofAperture":     return apertureToStr(settings.dofFocalLength, settings.dofDistance);
            case "dofPlane":        return toStr(settings.dofPlane);
        }
        return null;
    }
    // ------------------------------------------------------------------------
    // ------------------------------------------------------------------------
    private function setApertureSettings(strValue: String) {
        var dofFocalLength, dofDistance: float;
        var str1, str2 : String;
        var success: Bool;

        success = StrSplitFirst(strValue, "|", str1, str2);

        dofFocalLength = StringToFloat(str1, -666.6);
        dofDistance = StringToFloat(str2, -666.6);

        if (success && dofFocalLength != -666.6 && dofDistance != -666.6) {
            theCam.setAperture(dofFocalLength, dofDistance);
        }
    }
    // ------------------------------------------------------------------------
    public function setValue(id: string, v: SSBUI_Value) {
        var settings: SStoryBoardCameraSettings;

        settings = theCam.getSettings();

        switch (id) {
            case "pos":                 theCam.setPosition(v.vec); return;
            case "rot":                 theCam.setRotation(EulerAngles(v.vec.X, v.vec.Y, v.vec.Z)); return;
            case "fov":                 theCam.setFov(v.f); return;
            case "zoom":                theCam.setZoom(v.f); return;
            case "dofIntensity":        theCam.setDofIntensity(v.f); return;
            case "dofFocusFar":         theCam.setFocus(settings.dof.focusNear, v.f); return;
            case "dofFocusNear":        theCam.setFocus(v.f, settings.dof.focusFar); return;
            case "dofBlurFar":          theCam.setBlur(settings.dof.blurNear, v.f); return;
            case "dofBlurNear":         theCam.setBlur(v.f, settings.dof.blurFar); return;
            case "dofAperture":         setApertureSettings(v.s); return;
            case "dofPlane":            theCam.setDofPlane(v.s); return;
            default:
                break;
        }
    }
    // ------------------------------------------------------------------------
    private function adjust(
        v: SSBUI_Value, delta: float, min: float, max: float) : SSBUI_Value
    {
        if (min == max) {
            min = -100000.0;
            max = 100000.0;
        }
        switch (v.type) {
            case ESBUI_FLOAT:    v.f = ClampF(v.f + delta, min, max); break;
            case ESBUI_INT:      v.i = RoundF(ClampF(v.i + delta, min, max)); break;
        }
        return v;
    }
    // ------------------------------------------------------------------------
    public function adjustValue(
        id: String, delta: float, min: float, max: float) : SSBUI_Value
    {
        var v: SSBUI_Value;
        v = adjust(getValue(id), delta, min, max);
        setValue(id, v);
        return v;
    }
    // ------------------------------------------------------------------------
    public function getCurrentValue(id: String) : SSBUI_Value {
        var s: SSBUI_Value;
        var d: SSBUI_Value;

        s = getValue(id);

        // copy the relevant data
        d.type = s.type;
        switch (s.type) {
            case ESBUI_VECTOR:   d.vec = s.vec; break;
            case ESBUI_FLOAT:    d.f = s.f; break;
            case ESBUI_INT:      d.i = s.i; break;
            case ESBUI_STRING:   d.s = s.s; break;
        }
        return d;
    }
    // ------------------------------------------------------------------------
    public function setCurrentValue(id: String, newValue: SSBUI_Value) {
        var d: SSBUI_Value;

        d = getValue(id);

        // copy the relevant data
        switch (d.type) {
            case ESBUI_VECTOR:   d.vec = newValue.vec; break;
            case ESBUI_FLOAT:    d.f = newValue.f; break;
            case ESBUI_INT:      d.i = newValue.i; break;
            case ESBUI_STRING:   d.s = newValue.s; break;
        }
        setValue(id, d);
    }
    // ------------------------------------------------------------------------
    public function getFormattedValue(id: String) : String {
        var settings: SStoryBoardCameraSettings;
        var s: String;

        if (id == "dofAperture") {
            settings = theCam.getSettings();

            s = GetLocStringByKeyExt("SBUI_lDofFocalLength");
            s += NoTrailZeros(settings.dofFocalLength);
            s += "  ";
            s += GetLocStringByKeyExt("SBUI_lDofDistance");
            s += NoTrailZeros(settings.dofDistance);

            return s;
        } else {
            return formatValue(getValue(id));
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
abstract class CSbUiListSetting {
    // ------------------------------------------------------------------------
    protected var valueListId: String;
    protected var workModeState: CName;
    // ------------------------------------------------------------------------
    protected var setting: SSBUI_CamSetting;
    protected var value: SSBUI_Value;
    // ------------------------------------------------------------------------
    public function init(setting: SSBUI_CamSetting, value: SSBUI_Value) {
        this.setting = setting;
        this.value = value;
    }
    // ------------------------------------------------------------------------
    public function getWorkmodeState() : CName {
        return workModeState;
    }
    // ------------------------------------------------------------------------
    public function getValueListId() : String {
        return valueListId;
    }
    // ------------------------------------------------------------------------
    public function getValueId() : String {
        return this.value.s;
    }
    // ------------------------------------------------------------------------
    public function setValueId(valueId: String) {
        this.value.s = valueId;
    }
    // ------------------------------------------------------------------------
    public function getValue() : SSBUI_Value {
        return this.value;
    }
    // ------------------------------------------------------------------------
    public function getId() : String {
        return setting.sid;
    }
    // ------------------------------------------------------------------------
    public function getCaption() : String {
        return setting.caption;
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
class CSbUiDofPlaneCamSetting extends CSbUiListSetting {
    default workModeState = 'SbUi_DofPlaneSelection';
    default valueListId = "DofPlane";
}
// ----------------------------------------------------------------------------
class CSbUiDofApertureCamSetting extends CSbUiListSetting {
    default workModeState = 'SbUi_DofApertureSelection';
    default valueListId = "DofAperture";
}
// ----------------------------------------------------------------------------
