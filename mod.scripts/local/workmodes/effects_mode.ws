// -----------------------------------------------------------------------------
//
// BUGS:
//
// TODO:
//
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
state SbUi_AssetEffects in CModStoryBoardEffectsMode extends SbUi_FilteredListSelect
{
    // ------------------------------------------------------------------------
    protected var asset: CModStoryBoardAsset;
    private var newEffect: SStoryBoardEffectsSettings;
    private var stopPrevious: bool;

    default stopPrevious = true;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        var shotSettings: SStoryBoardShotAssetSettings;

        parent.view.title = parent.getName();
        parent.view.statsLabel = GetLocStringByKeyExt("SBUI_SelectEffectsListTitle");
        // update fields if the menu is already open
        parent.view.listMenuRef.setTitle(parent.view.title);
        parent.view.listMenuRef.setStatsLabel(parent.view.statsLabel);

        asset = parent.assetManager.getSelectedAsset();
        listProvider = parent.effectsListsManager.getEffectsListForAsset(asset);

        shotSettings = asset.getShotSettings();
        newEffect = shotSettings.effects;

        if (newEffect.effectId == "") {
            newEffect.effectId = "-1";
        }
        listProvider.setSelection(newEffect.effectId, true);

        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        parent.OnHotkeyHelp(hotkeyList);

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ResetEffects'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectPrev', "SBUI_SelectPrevEffect"));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectNext', "SBUI_SelectNextEffect"));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SwitchStackEffects'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_Back', "SBUI_BackToActorOverview"));

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleSpecialWorkmodeCam', "SBUI_ToggleAssetsCamera"));
    }
    // ------------------------------------------------------------------------
    event OnSelected(optionId: String) {
        var effectName: CName;

        if (listProvider.setSelection(optionId, true)) {

            // selection was "real"
            newEffect.effectId = optionId;
            newEffect.effectName = parent.effectsListsManager.getEffectName(newEffect.effectId);
            asset.setEffect(newEffect);

            if (parent.theEffectsDirector.startEffectForAsset(asset, stopPrevious))
            {
                parent.notice(GetLocStringByKeyExt("SBUI_iEffectStart") + "<font color=\"#005011\">" + newEffect.effectName + "</font>");
            } else {
                // --- failed to start effect (not exists in entity or other reason)
                parent.error(GetLocStringByKeyExt("SBUI_iEffectError") + newEffect.effectName);
            }
        }
        updateView();
    }
    // ------------------------------------------------------------------------
    event OnSwitchStackEffects(action: SInputAction) {
        if (IsPressed(action)) {
            stopPrevious = !stopPrevious;
            if (stopPrevious) {
                parent.theEffectsDirector.startEffectForAsset(asset, stopPrevious);
                parent.notice(GetLocStringByKeyExt("SBUI_iStackEffectsOff"));
            } else {
                parent.notice(GetLocStringByKeyExt("SBUI_iStackEffectsOn"));
            }
        }
    }
    // ------------------------------------------------------------------------
    event OnResetEffects(action: SInputAction) {
        if (IsPressed(action)) {
            OnSelected("-1");
            parent.notice(GetLocStringByKeyExt("SBUI_ResetEffects"));
        }
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        super.registerListeners();

        theInput.RegisterListener(this, 'OnBack', 'SBUI_AcceptChanges');
        theInput.RegisterListener(this, 'OnResetEffects', 'SBUI_ResetEffects');
        theInput.RegisterListener(this, 'OnSwitchStackEffects', 'SBUI_SwitchStackEffects');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        super.unregisterListeners();

        theInput.UnregisterListener(this, 'SBUI_AcceptChanges');
        theInput.UnregisterListener(this, 'SBUI_ResetEffects');
        theInput.UnregisterListener(this, 'SBUI_SwitchStackEffects');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Select asset (actor/item)
// -------------------------------------------
state SbUi_EffectsAssetSelection in CModStoryBoardEffectsMode
    extends SbUi_AssetSelection
{
    // ------------------------------------------------------------------------
    default listTitleKey = "SBUI_EffectsListTitle";
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        super.OnEnterState(prevStateName);

        theInput.RegisterListener(this, 'OnStartEffectsSelection', 'SBUI_SetupEffects');
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        theInput.UnregisterListener(this, 'SBUI_SetupEffects');

        super.OnLeaveState(nextStateName);
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        super.OnHotkeyHelp(hotkeyList);
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SetupEffects'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleSpecialWorkmodeCam', "SBUI_ToggleAssetsCamera"));
    }
    // ------------------------------------------------------------------------
    event OnStartEffectsSelection(action: SInputAction) {
        if (IsPressed(action)) {
            parent.PushState('SbUi_AssetEffects');
        }
    }
    // ------------------------------------------------------------------------
    protected function switchToSpecialCam() {
        parent.OnSwitchToSpecialCam();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Ui for selecting effects of assets for currently selected
// storyboard shot.
//
statemachine class CModStoryBoardEffectsMode extends CModStoryBoardAssetSelectionBasedWorkMode
{
    default workMode = 'SBUI_ModeEffects';
    default workContext = 'MOD_StoryBoardUi_ModeEffects';
    default generalHelpKey = "SBUI_EffectsGeneralHelp";
    default defaultState = 'SbUi_EffectsAssetSelection';
    // ------------------------------------------------------------------------
    // manages the set of compatible effects to choose from
    protected var effectsListsManager: CModStoryBoardEffectsListsManager;
    // re/starts/stops the effects for selected assets
    protected var theEffectsDirector: CModStoryBoardEffectsDirector;
    // ------------------------------------------------------------------------
    public function init(storyboard: CModStoryBoard) {
        super.init(storyboard);

        effectsListsManager = storyboard.getEffectsListsManager();
        theEffectsDirector = shotViewer.getEffectsDirector();
    }
    // ------------------------------------------------------------------------
    public function activate(shot: CModStoryBoardShot) {
        super.activate(shot);

        effectsListsManager.activate();
    }
    // ------------------------------------------------------------------------
    event OnSwitchToSpecialCam() {

        // --- change view to see currently selected asset
        shotViewer.switchCamTo(
            SBUI_createCamSettingsFor(
                SBUICam_AssetPreview, assetManager.getSelectedAsset())
        );

    }
    // ------------------------------------------------------------------------
    public function storeSettings() {
        // Note: this overwrites *all* asset settings in the shot
        storyboard.storeCurrentAssetSettingsIn(shot);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
