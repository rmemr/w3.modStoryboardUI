// -----------------------------------------------------------------------------
//
// BUGS:
//  - toggleSnapToGround does not work if asset is far away from ground (> 1m?)
//      -> recalculate Z to snap on activating toggle (and adjust help if it works)?
//
// TODO:
//  - add some visible marker for selected asset (floating orb above asset?)
//
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
class CModStoryBoardInteractivePlacement extends CEntity {
    // ------------------------------------------------------------------------
    protected var placement: SStoryBoardPlacementSettings;
    protected var asset: CModStoryBoardAsset;
    // ------------------------------------------------------------------------
    private var isInteractiveMode: bool;
    private var snapToGround: bool;
    private var isRotationMode: bool;
    private var rotationMode: int;

    private var stepMoveSize: float; default stepMoveSize = 0.0075;
    private var stepRotSize: float; default stepRotSize = 0.1;
    private var stepPadSize: float; default stepPadSize = 10.0;

    private var camHeading: float;

    private var theWorld: CWorld;
    // ------------------------------------------------------------------------
    public function refreshActorPlacement() {
        var shotSettings: SStoryBoardShotAssetSettings;

        shotSettings = asset.getShotSettings();
        placement = shotSettings.placement;
    }
    // ------------------------------------------------------------------------
    public function startInteractiveMode(selectedAsset: CModStoryBoardAsset) {
        if (!isInteractiveMode) {
            theWorld = theGame.GetWorld();

            asset = selectedAsset;
            // freeze asset while moving/rotating
            asset.freeze();

            refreshActorPlacement();

            // repeats & overrideExisting = true
            AddTimer('updateInteractiveSettings', 0.015f, true, , , , true);
            isInteractiveMode = true;
        }
    }
    // ------------------------------------------------------------------------
    public function stopInteractiveMode() {
        RemoveTimer('updateInteractiveSettings');
        isInteractiveMode = false;
        // unfreeze after moving/rotation is done
        asset.unfreeze();
    }
    // ------------------------------------------------------------------------
    private var isBirdsEyeView : bool; default isBirdsEyeView = false;
    public function GetBirdsEye():bool{return this.isBirdsEyeView;}
    public function SetBirdsEye(set:bool){this.isBirdsEyeView = set;}
    // ------------------------------------------------------------------------
    timer function updateInteractiveSettings(deltaTime: float, id: int) {
        var newPlacement: SStoryBoardPlacementSettings;
        var directionFB, directionLR: float;
        var moveFB, moveLR, moveUD, moveFBPad, moveLRPad: float;
        var rotateLR, rotateLRPad: float;
        var groundZ: float;
        var camera : CStaticCamera;
        var camPos : Vector;


        newPlacement = placement;

        if (isRotationMode) {
            rotateLR = theInput.GetActionValue('GI_MouseDampX');

            switch (rotationMode) {
                case 0: newPlacement.rot.Yaw -= (rotateLR + rotateLRPad) * stepRotSize; break;
                case 1: newPlacement.rot.Pitch -= (rotateLR + rotateLRPad) * stepRotSize; break;
                case 2: newPlacement.rot.Roll -= (rotateLR + rotateLRPad) * stepRotSize; break;
            }

        } else {

            moveLR = theInput.GetActionValue('GI_MouseDampX');
            moveFB = - theInput.GetActionValue('GI_MouseDampY');

            moveUD = theInput.GetActionValue('SBUI_AssetMoveUpDown');

            // Gamepad Support
            moveLRPad = stepPadSize * theInput.GetActionValue('SBUI_AxisLeftX');
            moveFBPad = stepPadSize * theInput.GetActionValue('SBUI_AxisLeftY');
            rotateLRPad = -stepPadSize * theInput.GetActionValue('SBUI_AxisRightX');
            newPlacement.rot.Yaw -= (rotateLR + rotateLRPad) * stepRotSize;

            // use current set cam heading so movement is aligned with visuals
            directionFB = Deg2Rad(camHeading + 90);
            directionLR = Deg2Rad(camHeading);

            newPlacement.pos.X += (moveFB + moveFBPad) * stepMoveSize * CosF(directionFB)
                                + (moveLR + moveLRPad) * stepMoveSize * CosF(directionLR);

            newPlacement.pos.Y += (moveFB + moveFBPad) * stepMoveSize * SinF(directionFB)
                                + (moveLR + moveLRPad) * stepMoveSize * SinF(directionLR);

            // somehow this is much slower than with mouse adjustments
            newPlacement.pos.Z += moveUD * stepMoveSize * 4;

            // deactivate snaptoGround when up/down hotkeys are used
            if (moveUD != 0) {
                snapToGround = false;
            }

            // adjust to world surface
            if (snapToGround && theWorld.PhysicsCorrectZ(newPlacement.pos, groundZ))
            {
                newPlacement.pos.Z = groundZ;
            }
        }

        if (newPlacement != placement) {
            asset.setPlacement(newPlacement);
            placement = newPlacement;

            if(isBirdsEyeView){
                camera = (CStaticCamera)theCamera.GetTopmostCameraObject();
                camPos = camera.GetWorldPosition();
                camPos.X = placement.pos.X;
                camPos.Y = placement.pos.Y;
                camera.Teleport(camPos);
            }
        }
    }
    // ------------------------------------------------------------------------
    public function setCameraHeading(newHeading: float) {
        camHeading = newHeading;
    }
    // ------------------------------------------------------------------------
    public function setStepSize(move: float, rot: float) {
        stepMoveSize = move;
        stepRotSize = rot;
    }
    // ------------------------------------------------------------------------
    public function getStepSize(out moveStep: float, out rotStep: float) {
        moveStep = stepMoveSize;
        rotStep = stepRotSize;
    }
    // ------------------------------------------------------------------------
    public function activateSnapToGound(activate: bool) {
        snapToGround = activate;
    }
    // ------------------------------------------------------------------------
    public function isSnapToGroundActive() : bool {
        return snapToGround;
    }
    // ------------------------------------------------------------------------
    public function activateRotationMode(activate: bool) {
        isRotationMode = activate;
        // always reset to yaw as default
        rotationMode = 0;
    }
    // ------------------------------------------------------------------------
    public function setRotationMode(mode: int) {
        isRotationMode = true;
        rotationMode = mode % 3;
    }
    // ------------------------------------------------------------------------
    public function getRotationMode() : int {
        return rotationMode;
    }
    // ------------------------------------------------------------------------
    public function isRotationMode() : bool {
        return isRotationMode;
    }
    // ------------------------------------------------------------------------
    public function resetRotation() {
        var newPlacement: SStoryBoardPlacementSettings;
        var camera : CStaticCamera;
        var camPos : Vector;

        newPlacement = placement;
        newPlacement.rot = EulerAngles(0, 0, 0);

        if (newPlacement != placement) {
            asset.setPlacement(newPlacement);
            placement = newPlacement;

            if(isBirdsEyeView){
                camera = (CStaticCamera)theCamera.GetTopmostCameraObject();
                camPos = camera.GetWorldPosition();
                camPos.X = placement.pos.X;
                camPos.Y = placement.pos.Y;
                camera.Teleport(camPos);
            }
        }
    }
    // ------------------------------------------------------------------------
    public function activate() {
    }
    // ------------------------------------------------------------------------
    public function deactivate() {
    }
    // ------------------------------------------------------------------------
}

state SbUi_AssetPlacement in CModStoryBoardPlacementMode
{
    // ------------------------------------------------------------------------
    private var moveStepSize: float;
    private var rotStepSize: float;
    private var isMovementMode: bool;
    private var rotationMode: int;
    // ------------------------------------------------------------------------
    private var newPlacement: SStoryBoardPlacementSettings;
    private var asset: CModStoryBoardAsset;

    private var lastFacedAssetId: String;
    // alias
    private var theController: CModStoryBoardInteractivePlacement;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        var shotSettings: SStoryBoardShotAssetSettings;

        parent.showUi(false);

        asset = parent.assetManager.getSelectedAsset();

        theController = parent.theController;
        theController.startInteractiveMode(asset);

        theController.getStepSize(moveStepSize, rotStepSize);

        // default is no snap to ground to prevent auto changing
        theController.activateSnapToGound(false);

        shotSettings = asset.getShotSettings();
        newPlacement = shotSettings.placement;

        theController.setCameraHeading(parent.shotViewer.getCameraHeading());

        theInput.RegisterListener(this, 'OnChangePlacementSpeed', 'SBUI_ToggleFast');
        theInput.RegisterListener(this, 'OnChangePlacementSpeed', 'SBUI_ToggleSlow');
        theInput.RegisterListener(this, 'OnBack', 'SBUI_AcceptChanges');
        theInput.RegisterListener(this, 'OnToggleSnapToGround', 'SBUI_ToggleSnapToGround');
        theInput.RegisterListener(this, 'OnToggleRotation', 'SBUI_ToggleMoveRotate');
        theInput.RegisterListener(this, 'OnSwitchRotationMode', 'SBUI_SwitchRotateMode');
        theInput.RegisterListener(this, 'OnResetRotation', 'SBUI_ResetRotation');
        theInput.RegisterListener(this, 'OnCycleAutoFacing', 'SBUI_AssetCycleActorFacing');
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        theController.stopInteractiveMode();

        // some asset may need a "fresh" start
        if (asset.needsRespawn()) {
            asset.respawn();
        }

        theInput.UnregisterListener(this, 'SBUI_ToggleFast');
        theInput.UnregisterListener(this, 'SBUI_ToggleSlow');
        theInput.UnregisterListener(this, 'SBUI_AcceptChanges');
        theInput.UnregisterListener(this, 'SBUI_ToggleSnapToGround');
        theInput.UnregisterListener(this, 'SBUI_ToggleMoveRotate');
        theInput.UnregisterListener(this, 'SBUI_SwitchRotateMode');
        theInput.UnregisterListener(this, 'SBUI_ResetRotation');
        theInput.UnregisterListener(this, 'SBUI_AssetCycleActorFacing');
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        parent.OnHotkeyHelp(hotkeyList);

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_Back', "SBUI_BackToOverview"));

        hotkeyList.PushBack(HotkeyHelp_from('GI_MouseDampX', "SBUI_AssetMoveLeftRight"));
        hotkeyList.PushBack(HotkeyHelp_from('GI_MouseDampY', "SBUI_AssetMoveForwardBack"));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_AssetMoveUpDown'));
        hotkeyList.PushBack(HotkeyHelp_from('GI_MouseDampX', "SBUI_AssetRot"));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleMoveRotate'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SwitchRotateMode'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ResetRotation'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleSnapToGround'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_AssetCycleActorFacing'));

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_AcceptChanges', "SBUI_AcceptPosition"));

        // allow switching between asset directly in placement state, too
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectPrev'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectNext'));

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleSpecialWorkmodeCam', "SBUI_TogglePlacementCamera"));

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleFast'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleSlow'));
    }
    // ------------------------------------------------------------------------
    event OnToggleSpecialCam(action: SInputAction) {
        parent.OnToggleSpecialCam(action);

        // update the heading of the currently set camera to align movement with
        // displayed plane
        theController.setCameraHeading(parent.shotViewer.getCameraHeading());
    }
    // ------------------------------------------------------------------------
    event OnToggleRotation(action: SInputAction) {
        var msgKey: String;
        if (IsPressed(action)) {
            if (!theController.isRotationMode()) {
                theController.setRotationMode(0);
                msgKey = "SBUI_iRotationModeYaw";
            } else {
                theController.activateRotationMode(false);
                msgKey = "SBUI_iMovementMode";
            }
            parent.notice(GetLocStringByKeyExt(msgKey));
        }
    }
    // ------------------------------------------------------------------------
    event OnSwitchRotationMode(action: SInputAction) {
        var msgKey: String;
        if (IsPressed(action)) {
            theController.setRotationMode((theController.getRotationMode() + 1) % 3);

            switch (theController.getRotationMode()) {
                case 0: msgKey = "SBUI_iRotationModeYaw"; break;
                case 1: msgKey = "SBUI_iRotationModePitch"; break;
                case 2: msgKey = "SBUI_iRotationModeRoll"; break;
            }
            parent.notice(GetLocStringByKeyExt(msgKey));
        }
    }
    // ------------------------------------------------------------------------
    event OnResetRotation(action: SInputAction) {
        var msgKey: String;
        if (IsPressed(action)) {
            theController.resetRotation();
            parent.notice(GetLocStringByKeyExt("SBUI_iRotationReseted"));
        }
    }
    // ------------------------------------------------------------------------
    event OnToggleSnapToGround(action: SInputAction) {
        var msgKey: String;
        if (IsPressed(action)) {
            theController.activateSnapToGound(!theController.isSnapToGroundActive());

            if (theController.isSnapToGroundActive()) {
                // TODO recalculate Z to snap (cause the PhysicsCorrectZ method
                // works only near gorund correctly)
                msgKey = "SBUI_iSnapToGroundOn";
            } else {
                msgKey = "SBUI_iSnapToGroundOff";
            }
            parent.notice(GetLocStringByKeyExt(msgKey));
        }
    }
    // ------------------------------------------------------------------------
    event OnCycleAutoFacing(action: SInputAction) {
        var facedAsset: CModStoryBoardAsset;
        var actor: CModStoryBoardActor;

        actor = (CModStoryBoardActor)asset;

        if (IsPressed(action) && actor) {

            lastFacedAssetId = parent.assetManager.getNextInteractionActorId(
                actor.getId(), lastFacedAssetId);

            // make sure there is another one
            if (lastFacedAssetId != actor.getId()) {
                facedAsset = parent.assetManager.getAsset(lastFacedAssetId);

                actor.rotateToFace(facedAsset);
                // trigger controller to refetch current actor positioning
                theController.refreshActorPlacement();

                parent.notice(GetLocStringByKeyExt("SBUI_iFacingActor") +
                    facedAsset.getName());
            }

        }
    }
    // ------------------------------------------------------------------------
    event OnSelected(optionId: String) {
        var shotSettings: SStoryBoardShotAssetSettings;

        // some assets may need a "fresh" start
        if (asset.needsRespawn()) {
            asset.respawn();
        }

        parent.assetManager.selectAsset(optionId);

        asset = parent.assetManager.getSelectedAsset();
        newPlacement = shotSettings.placement;
        // restart interactive mode for the new asset
        theController.stopInteractiveMode();
        theController.startInteractiveMode(asset);

        // special cam may be actor dependent -> update
        if (parent.isSpecialCamInUse) {
            parent.OnSwitchToSpecialCam();
        }

        parent.notice(GetLocStringByKeyExt("SBUI_iSelectedAssetInfo")
                + asset.getName());
    }
    // ------------------------------------------------------------------------
    event OnChangePlacementSpeed(action: SInputAction) {
        if (IsPressed(action)) {
            if (action.aName == 'SBUI_ToggleFast') {
                // --- fast ---
                moveStepSize = 0.02;
                rotStepSize = 0.2;
                theController.setStepSize(moveStepSize, rotStepSize);
            } else {
                // --- slow ---
                moveStepSize = 0.0025;
                rotStepSize = 0.025;
                theController.setStepSize(moveStepSize, rotStepSize);
            }
        } else if (IsReleased(action)) {
            // -- normal ---
            moveStepSize = 0.0075;
            rotStepSize = 0.1;
            theController.setStepSize(moveStepSize, rotStepSize);
        }
    }
    // ------------------------------------------------------------------------
    event OnBack(action: SInputAction) {
        if (IsPressed(action)) {
            parent.PopState();
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
state SbUi_PlacementPresetSelection in CModStoryBoardPlacementMode
    extends SbUi_FilteredListSelect
{
    // ------------------------------------------------------------------------
    //private var newPlacement: SStoryBoardPlacementSettings;
    //private var asset: CModStoryBoardAsset;
    private var actorAssets: array<CModStoryBoardActor>;
    private var customPlacement: array<SStoryBoardPlacementSettings>;
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        var i, actorCount : int;

        parent.view.title = parent.getName();
        parent.view.statsLabel = GetLocStringByKeyExt("SBUI_SelectPlacementPresetsListTitle");
        // update fields if the menu is already open
        parent.view.listMenuRef.setTitle(parent.view.title);
        parent.view.listMenuRef.setStatsLabel(parent.view.statsLabel);

        actorCount = parent.assetManager.getActorCount();
        actorAssets = parent.assetManager.getActorAssets();
        listProvider = parent.dialogsetPresetsListsManager.getPlacementPresetsList(actorCount);

        // default selection is "current placement / no preset"
        listProvider.setSelection("0", true);

        // store settings of current actor placements for reset-to-custom options
        for (i = 0; i < actorCount; i += 1) {
            customPlacement.PushBack(actorAssets[i].getCurrentPlacement());
        }

        if (parent.isSpecialCamInUse) {
            OnSwitchToSpecialCam();
        }

        super.OnEnterState(prevStateName);
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        super.OnHotkeyHelp(hotkeyList);

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ResetPositions'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_Back', "SBUI_BackToOverview"));

        // allow switching between animations
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectPrev'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectNext'));

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleSpecialWorkmodeCam', "SBUI_TogglePlacementCamera"));
    }
    // ------------------------------------------------------------------------
    event OnSwitchToSpecialCam() {
        // constant overview cam
        parent.shotViewer.switchCamTo(
            SBUI_createCamSettingsFor(SBUICam_BirdsEyeViewSceneOrigin,
                ,, parent.thePlacementDirector.getOriginPlacement()));
    }
    // ------------------------------------------------------------------------
    event OnSelected(optionId: String) {
        var preset: SSbUiPlacementPreset;
        var placement: SStoryBoardPlacementSettings;
        var i: int;

        if (listProvider.setSelection(optionId, true)) {
            if (optionId == "0") {
                resetPositions();
            } else {
                preset = ((CModSbUiPlacementPresetsList)listProvider).getPlacementsForPreset(optionId);

                for (i = 0; i < actorAssets.Size(); i += 1) {
                    if (preset.slot.Size() > i) {
                        placement = SStoryBoardPlacementSettings(
                            VecTransform(
                                MatrixBuiltRotation(EulerAngles(
                                    parent.origin.rot.Pitch, parent.origin.rot.Yaw, parent.origin.rot.Roll
                                )),
                                preset.slot[i].pos
                            ) + parent.origin.pos,
                            EulerAngles(
                                AngleNormalize(parent.origin.rot.Pitch + preset.slot[i].rot.Pitch),
                                AngleNormalize(parent.origin.rot.Yaw + preset.slot[i].rot.Yaw),
                                AngleNormalize(parent.origin.rot.Roll + preset.slot[i].rot.Roll)
                            )
                        );
                        actorAssets[i].setPlacement(placement);
                    }
                }
                parent.notice(GetLocStringByKeyExt("SBUI_iSelectedPlacementPreset")
                    + preset.caption);

                parent.shot.setDialogsetId(optionId);
            }
        }
        updateView();
    }
    // ------------------------------------------------------------------------
    private function resetPositions() {
        var i: int;

        for (i = 0; i < actorAssets.Size(); i += 1) {
            actorAssets[i].setPlacement(customPlacement[i]);
        }
        listProvider.setSelection("0", true);
        //reset dialogset, too?
        //parent.shot.setDialogsetId("0");
        updateView();
    }
    // ------------------------------------------------------------------------
    event OnResetPositions(action: SInputAction) {
        if (IsPressed(action)) {
            resetPositions();
        }
    }
    // ------------------------------------------------------------------------
    protected function registerListeners() {
        super.registerListeners();

        theInput.RegisterListener(this, 'OnResetPositions', 'SBUI_ResetPositions');
    }
    // ------------------------------------------------------------------------
    protected function unregisterListeners() {
        super.unregisterListeners();

        theInput.UnregisterListener(this, 'SBUI_ResetPositions');
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Select asset (actor/item)
// rootstate == OnBack event triggers change to Overview Mode
state SbUi_PlacementAssetSelection in CModStoryBoardPlacementMode
    extends SbUi_AssetSelection
{
    // ------------------------------------------------------------------------
    default listTitleKey = "SBUI_AssetsListTitle";
    // ------------------------------------------------------------------------
    event OnEnterState(prevStateName: CName) {
        super.OnEnterState(prevStateName);

        theInput.RegisterListener(this, 'OnStartAssetPlacement', 'SBUI_ChangePlacement');
        theInput.RegisterListener(this, 'OnSelectPlacementPreset', 'SBUI_SelectPlacementPreset');
    }
    // ------------------------------------------------------------------------
    event OnLeaveState(nextStateName: CName) {
        theInput.UnregisterListener(this, 'SBUI_ChangePlacement');
        theInput.UnregisterListener(this, 'SBUI_SelectPlacementPreset');

        super.OnLeaveState(nextStateName);
    }
    // ------------------------------------------------------------------------
    event OnHotkeyHelp(out hotkeyList: array<SModUiHotkeyHelp>) {
        super.OnHotkeyHelp(hotkeyList);

        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ChangePlacement'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectPlacementPreset'));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_SelectAsset', "SBUI_SelectForPlacement"));
        hotkeyList.PushBack(HotkeyHelp_from('SBUI_ToggleSpecialWorkmodeCam', "SBUI_TogglePlacementCamera"));
    }
    // ------------------------------------------------------------------------
    event OnSelectPlacementPreset(action: SInputAction) {
        if (IsPressed(action)) {
            parent.PushState('SbUi_PlacementPresetSelection');
        }
    }
    // ------------------------------------------------------------------------
    event OnStartAssetPlacement(action: SInputAction) {
        if (IsPressed(action)) {
            parent.PushState('SbUi_AssetPlacement');
        }
    }
    // ------------------------------------------------------------------------
    protected function switchToSpecialCam() {
        parent.OnSwitchToSpecialCam();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// Ui for adjusting placement of assets (actors/items) for currently selected
// storyboard shot.
//  - changing currently selected asset
//  - providing special (predefined) toggle cam for viewing the whole scene
//  - placement of assets for this shot
//
statemachine class CModStoryBoardPlacementMode extends CModStoryBoardAssetSelectionBasedWorkMode
{
    default workMode = 'SBUI_ModePlacement';
    default workContext = 'MOD_StoryBoardUi_ModePlacement';
    default generalHelpKey = "SBUI_PlacementGeneralHelp";
    default defaultState = 'SbUi_PlacementAssetSelection';
    // ------------------------------------------------------------------------
    // manages the set of dialogset placment presets to choose from
    protected var dialogsetPresetsListsManager: CModStoryBoardDialogsetPresetsListsManager;
    // ------------------------------------------------------------------------
    protected var thePlacementDirector: CModStoryBoardPlacementDirector;
    protected var theController: CModStoryBoardInteractivePlacement;
    protected var origin: SStoryBoardOriginStateData;
    protected var originMarker: CEntity;
    // ------------------------------------------------------------------------
    public function init(storyboard: CModStoryBoard) {
        super.init(storyboard);
        dialogsetPresetsListsManager = storyboard.getDialogsetPresetsListsManager();
        origin = storyboard.getOrigin();
    }
    // ------------------------------------------------------------------------
    private function createPlacementController() : CModStoryBoardInteractivePlacement
    {
        var ent: CEntity;
        var template: CEntityTemplate;

        template = (CEntityTemplate)LoadResource("dlc\modtemplates\storyboardui\interactive_placement.w2ent", true);
        ent = theGame.CreateEntity(template,
            thePlayer.GetWorldPosition(), thePlayer.GetWorldRotation());

        return (CModStoryBoardInteractivePlacement)ent;
    }
    // ------------------------------------------------------------------------
    public function activate(shot: CModStoryBoardShot) {
        var template: CEntityTemplate;

        super.activate(shot);

        theController = createPlacementController();

        // easiest way to reference this entity from other classes
        theController.AddTag('SBUI_PlacementModeEntity');
        thePlacementDirector = shotViewer.getPlacementDirector();

        // spawn flag for current origin to mark the position dialogsets are
        // centered around
        template = (CEntityTemplate)LoadResource("engine\templates\editor\markers\review\opened_flag.w2ent", true);
        originMarker = theGame.CreateEntity(template, origin.pos, origin.rot);
    }
    // ------------------------------------------------------------------------
    public function deactivate() {
        super.deactivate();

        // despwan origin flag
        originMarker.Destroy();

        theController.Destroy();
        delete theController;
    }
    // ------------------------------------------------------------------------
    event OnSwitchToSpecialCam() {
        // constant overview cam
        shotViewer.switchCamTo(
            SBUI_createCamSettingsFor(SBUICam_BirdsEyeView,
                assetManager.getSelectedAsset(),,
                thePlacementDirector.getOriginPlacement()));
    }
    // ------------------------------------------------------------------------
    public function storeSettings() {
        // Note: this overwrites *all* asset settings in the shot (not just
        // placement settings!)
        storyboard.storeCurrentAssetSettingsIn(shot);
    }
    // ------------------------------------------------------------------------
    public function showUi(showUi: bool) {
        // show ui only in asset selection state
        if (showUi && GetCurrentStateName() != 'SbUi_AssetPlacement') {
            if (!view.listMenuRef) {
                theGame.RequestMenu('ListView', view);
            }
        } else {
            if (view.listMenuRef) {
                view.listMenuRef.close();
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
